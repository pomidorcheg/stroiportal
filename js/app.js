angular.module('app', [])

.controller('IndustryController', function($scope) {
    $scope.itemList = [true, false, false, false, false, false];
    $scope.tableList = [false, true, true, true, true, true];

    $scope.setActive = function(index) {
        resetLists();
        $scope.itemList[index] = true;
        $scope.tableList[index] = false;
    };

    function resetLists() {
        for (var i = 0, l = $scope.itemList.length; i < l; i++) {
            $scope.itemList[i] = false;
            $scope.tableList[i] = true;
        }
    }
})

.controller('CatalogController', function($scope) {
    $scope.itemList = [true, false, false, false, false, false];
    $scope.catalogList = [false, true, true, true, true, true];

    $scope.setActive = function(index) {
        resetLists();
        $scope.itemList[index] = true;
        $scope.catalogList[index] = false;
    };

    function resetLists() {
        for (var i = 0, l = $scope.itemList.length; i < l; i++) {
            $scope.itemList[i] = false;
            $scope.catalogList[i] = true;
        }
    }
})

.controller('SliderController', function($scope, $element) {
    var slides = $element[0].getElementsByClassName('feedback-slide');
    var container = $element[0].getElementsByClassName('feedbacks-container')[0];

    angular.element(slides[0]).addClass('active');

    $scope.slideNext = function() {
        for (var i = 0, l = slides.length; i < l; i++) {
            if (angular.element(slides[i]).hasClass('active')) {
                var index = i + 1 >= l ? 0 : i + 1;
                angular.element(container).css("transform","translateX(" + index * -805 + "px)");
                angular.element(slides[i]).removeClass('active');
                angular.element(slides[index]).addClass('active');
                break;
            }
        }
    };

    $scope.slidePrev = function() {
        for (var i = 0, l = slides.length; i < l; i++) {
            if (angular.element(slides[i]).hasClass('active')) {
                var index = i - 1 < 0 ? l - 1 : i - 1;
                angular.element(container).css("transform","translateX(" + index * -805 + "px)");
                angular.element(slides[i]).removeClass('active');
                angular.element(slides[index]).addClass('active');
                break;
            }
        }
    };
})

.controller('OrderItemController', function($scope, $element, $rootScope) {
    $scope.activeChat = false;
    $scope.activeCard = false;
    $scope.openedItem = false;

    $scope.$on('DYNAMIC_ACTIONS', function(event, target) {
        if (target !== $element) {
            $scope.activeCard = false;
            $scope.activeChat = false;
        }
    });

    $scope.toggleChat = function() {
        $rootScope.$broadcast('DYNAMIC_ACTIONS', $element);
        $scope.activeCard = false;
        $scope.activeChat = !$scope.activeChat;
    };

    $scope.toggleCard = function() {
        $rootScope.$broadcast('DYNAMIC_ACTIONS', $element);
        $scope.activeChat = false;
        $scope.activeCard = !$scope.activeCard;
    };

    $scope.toggleItem = function() {
        $scope.openedItem = !$scope.openedItem;
    };
})

.controller('ShareController', function($scope, $element) {
    $scope.hide = true;

    $scope.toggleShare = function() {
        $scope.hide = !$scope.hide;
    };
})

.controller('SystemController', function($scope, $element) {
    $scope.hide = true;

    $scope.toggleBlock = function() {
        $scope.hide = !$scope.hide;
    };
});